package be.lightinject.context;

import org.junit.Test;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.concurrent.Future;

import static org.assertj.core.api.Assertions.assertThat;

public class PredicatesTest {
    @Test
    public void alwaysTrue() throws Exception {
        assertThat(Predicates.alwaysTrue().test(new Object())).isTrue();
    }

    @Test
    public void classAnnotation_whenClassHasAnnotation() throws Exception {
        assertThat(Predicates.classAnnotation(Singleton.class).test(ClassWithAnnotation.class)).isTrue();
    }

    @Test
    public void classAnnotation_whenClassDoesNotHaveAnnotation() throws Exception {
        assertThat(Predicates.classAnnotation(Named.class).test(ClassWithAnnotation.class)).isFalse();
    }

    @Test
    public void descendantOf_whenExtends() throws Exception {
        assertThat(Predicates.descendantOf(Thread.class).test(ClassWithAnnotation.class)).isTrue();
    }

    @Test
    public void descendantOf_whenImplements() throws Exception {
        assertThat(Predicates.descendantOf(Runnable.class).test(ClassWithAnnotation.class)).isTrue();
    }

    @Test
    public void descendantOf_whenDoesNotExtendsAndNotImplements() throws Exception {
        assertThat(Predicates.descendantOf(Future.class).test(ClassWithAnnotation.class)).isFalse();
    }

    @Test
    public void notPrivate_whenInnerClass() throws Exception {
        assertThat(Predicates.notPrivate().test(ClassWithAnnotation.class)).isFalse();
    }

    @Test
    public void notPrivate_whenClass() throws Exception {
        assertThat(Predicates.notPrivate().test(Predicates.class)).isTrue();
    }

    @Singleton
    private class ClassWithAnnotation extends Thread implements Runnable {
        @Override
        public void run() {
        }
    }
}
