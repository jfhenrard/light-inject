package be.lightinject.context;

import org.junit.Test;

import javax.inject.Named;
import java.util.List;

import static be.lightinject.context.PackageScanner.packageScanner;
import static be.lightinject.context.Predicates.classAnnotation;
import static org.assertj.core.api.Assertions.assertThat;

public class PackageScannerTest {
    @Test
    public void scan() throws Exception {
        List<Class<?>> classes = packageScanner().from(this.getClass().getPackage().getName()).scan();

        assertThat(classes).contains(PackageScanner.class, PackageScannerTest.class);
    }

    @Test
    public void scan_withAnnotationCriteria() throws Exception {
        List<Class<?>> classes = packageScanner()
                                        .from(this.getClass().getPackage().getName())
                                        .with(classAnnotation(Named.class))
                                        .scan();

        classes.stream().forEach((clazz) -> System.out.println(clazz.getName()));
        assertThat(classes).containsOnly(ContextTest.FirstBean.class,
                                         ContextTest.SecondBean.class,
                                         ContextTest.DescendantABean.class,
                                         ContextTest.DescendantBBean.class);
    }
}
