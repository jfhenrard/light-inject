package be.lightinject.context;

import be.lightinject.context4test.ExternalBean;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ContextTest {
    @Before
    public void setUp() throws Exception {
        Context.packageScan("be.lightinject.context");
    }

    @Test
    public void packageScan() throws Exception {
        assertThat(toClasses(Context.getObjects())).contains(FirstBean.class, DescendantABean.class, DescendantBBean.class);

        Context.getBeans(Validated.class)
                .stream()
                .forEach((bean) -> {
                        bean.assertInjected();
                        bean.assertPostConstructed();
                    });
    }

    @Test
    public void packageScan_addSecondScan() throws Exception {
        Context.packageScan("be.lightinject.context4test");
        Context.getBean(ExternalBean.class);
    }

    @Test
    public void packageScan_scanTwoTimesSamePackage() throws Exception {
        Context.packageScan("be.lightinject.context");
        assertThat(toClasses(Context.getObjects())).contains(FirstBean.class, DescendantABean.class, DescendantBBean.class);
    }

    @Test
    public void clear() throws Exception {
        assertThat(Context.getObjects().size()).isGreaterThan(0);
        Context.clear();
        assertThat(Context.getObjects().size()).isZero();
    }

    @Test
    public void register_whenObjectIsNotYetPresent() throws Exception {
        Object myBean = new Object() {};

        Context.register(myBean);

        assertThat(Context.getBean(myBean.getClass())).isEqualTo(myBean);
        assertThat(Context.getBeans(Object.class)).contains(myBean);
    }

    @Test(expected = IllegalArgumentException.class)
    public void register_whenObjectIsAlreadyPresent() throws Exception {
        Object myBean = new Object() {};

        Context.register(myBean);
        Context.register(myBean);
    }

    @Test
    public void unregister_whenObjectIsAlreadyPresent() throws Exception {
        Object myBean = new Object() {};

        Context.register(myBean);
        assertThat(Context.getObjects().contains(myBean)).isTrue();

        Context.unregister(myBean);
        assertThat(Context.getObjects().contains(myBean)).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void unregister_whenObjectIsNotYetPresent() throws Exception {
        Object myBean = new Object() {};

        Context.unregister(myBean);
    }

    @Test
    public void getBeans() throws Exception {
        assertThat(toClasses(Context.getBeans(AbstractBean.class))).contains(DescendantABean.class, DescendantBBean.class);
        assertThat(toClasses(Context.getBeans(DescendantABean.class))).containsOnly(DescendantABean.class);
    }

    @Test(expected=RuntimeException.class)
    public void getBeans_whenBeanDoesNotExists() throws Exception {
        Context.getBeans(Context.class);
    }

    @Test
    public void getBean() throws Exception {
        assertThat(Context.getBean(FirstBean.class).getClass()).isEqualTo(FirstBean.class);
    }

    @Test(expected=RuntimeException.class)
    public void getBean_whenMultipleBeanExists() throws Exception {
        Context.getBean(AbstractBean.class);
    }

    @Test(expected=RuntimeException.class)
    public void getBean_whenBeanDoesNotExist() throws Exception {
        Context.getBean(Context.class);
    }

    @SuppressWarnings("unchecked")
    private static Iterator<Class<?>> toClasses(Collection<?> objects) {
        return objects.stream().map(Object::getClass).iterator();
    }

    public static interface Validated {
        @SuppressWarnings("UnusedDeclaration")
        void assertInjected();

        @SuppressWarnings("UnusedDeclaration")
        void assertPostConstructed();
    }

    public static class AbstractBean implements Validated {
        @Inject
        @SuppressWarnings("UnusedDeclaration")
        private SecondBean beanInSuper;

        private boolean postConstructed = false;

        @PostConstruct
        public void postConstructSuper() {
            postConstructed = true;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof AbstractBean)) return false;

            AbstractBean that = (AbstractBean) o;

            return postConstructed == that.postConstructed && beanInSuper.equals(that.beanInSuper);

        }

        @Override
        public int hashCode() {
            int result = beanInSuper.hashCode();
            result = 31 * result + (postConstructed ? 1 : 0);
            return result;
        }

        public void assertInjected() {
            assertThat(beanInSuper).isNotNull();
        }

        public void assertPostConstructed() {
            assertThat(postConstructed).isTrue();
        }
    }

    @Named
    public static class FirstBean implements Validated {
        @Inject
        @SuppressWarnings("UnusedDeclaration")
        private DescendantABean a;

        @Inject
        @SuppressWarnings({"MismatchedQueryAndUpdateOfCollection", "UnusedDeclaration"})
        private List<AbstractBean> list;

        private boolean postConstructed = false;

        @PostConstruct
        @SuppressWarnings("UnusedDeclaration")
        public void postConstruct() {
            postConstructed = true;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof FirstBean)) return false;

            FirstBean firstBean = (FirstBean) o;

            return postConstructed == firstBean.postConstructed
                    && a.equals(firstBean.a)
                    && list.equals(firstBean.list);

        }

        @Override
        public int hashCode() {
            int result = a.hashCode();
            result = 31 * result + list.hashCode();
            result = 31 * result + (postConstructed ? 1 : 0);
            return result;
        }

        public void assertInjected() {
            assertThat(a).isNotNull();
            assertThat(toClasses(list)).containsOnly(DescendantABean.class, DescendantBBean.class);
        }

        public void assertPostConstructed() {
            assertThat(postConstructed).isTrue();
        }
    }

    @Named
    public static class SecondBean {}

    @Named
    public static class DescendantABean extends AbstractBean {
        @Inject
        @SuppressWarnings("UnusedDeclaration")
        private SecondBean beanInDescendant;

        private boolean postConstructed = false;

        @PostConstruct
        @SuppressWarnings("UnusedDeclaration")
        public void postConstructA() {
            postConstructed = true;
        }

        public void assertInjected() {
            super.assertInjected();
            assertThat(beanInDescendant).isNotNull();
        }

        public void assertPostConstructed() {
            super.assertPostConstructed();
            assertThat(postConstructed).isTrue();
        }
    }

    @Named
    public static class DescendantBBean extends AbstractBean {
    }
}
