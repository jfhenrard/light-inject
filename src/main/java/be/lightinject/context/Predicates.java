package be.lightinject.context;

import java.lang.annotation.Annotation;
import java.util.function.Predicate;

public class Predicates {
    public static <T> Predicate<T> alwaysTrue() {
        return (object) -> true;
    }

    public static Predicate<Class<?>> classAnnotation(Class<? extends Annotation> annotation) {
        return (clazz) -> clazz.getAnnotation(annotation) != null;
    }

    public static Predicate<Class<?>> descendantOf(Class<?> ancestor) {
        return ancestor::isAssignableFrom;
    }

    public static Predicate<Class<?>> notPrivate() {
        return (clazz) -> !clazz.isAnonymousClass() && !clazz.isLocalClass() && !clazz.isMemberClass();
    }
}
