package be.lightinject.context;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableCollection;
import static java.util.logging.Logger.getLogger;

public final class Context {
    private static final Logger LOGGER = getLogger(Context.class.getName());

    private static final List<Object> singletons = new ArrayList<>();

    public static void clear() {
        singletons.clear();
    }

    public static void packageScan(String... packageNames) {
        List<Class<?>> classes = new ArrayList<>();
        asList(packageNames)
            .stream()
            .filter(Context::notAlreadyScanned)
            .forEach((packageName) -> classes.addAll(searchClassesIn(packageName)));

        List<Object> objects = asList(classes.stream()
                                             .parallel()
                                             .map(toInstances())
                                             .toArray());

        singletons.addAll(objects);

        objects.stream()
               .parallel()
               .forEach((object) -> injectInto(object.getClass(), object));

        objects.stream()
                .parallel()
                .forEach((object) -> executePostConstructor(object.getClass(), object));
    }

    private static List<Class<?>> searchClassesIn(String packageName) {
        List<Class<?>> classes = PackageScanner.packageScanner().from(packageName).with(Predicates.classAnnotation(Named.class)).scan();

        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.info("Founded for " + packageName + " : ");
            classes.stream()
                   .map(Class::getName)
                   .sorted((o1, o2) -> o1.compareTo(o2))
                   .forEach((className) -> LOGGER.info("           " + className));
        }
        return classes;
    }

    private static boolean notAlreadyScanned(String packageName) {
        return singletons.stream()
                         .parallel()
                         .map(Object::getClass)
                         .map((clazz) -> clazz.getPackage().getName())
                         .distinct()
                         .filter((classPackageName) -> classPackageName.equals(packageName))
                         .count() == 0;
    }

    private static Function<Class<?>, Object> toInstances() {
        return (clazz) -> {
            try {
                return clazz.newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    public static void injectInto(Object object) {
        injectInto(object.getClass(), object);
    }

    private static void injectInto(Class<?> clazz, Object object) {
        getInjectableFields(clazz).forEachRemaining((field) -> {
            boolean accessible = field.isAccessible();
            try {
                field.setAccessible(true);
                if (Collection.class.isAssignableFrom(field.getType())) {
                    field.set(object, getBeans(getGenericClazz(clazz, field)));
                } else {
                    field.set(object, getBean(field.getType()));
                }
            } catch (Exception e) {
                throw new IllegalStateException("Missing bean for " + clazz.getName() + "." + field.getName() + " in " + clazz.getName(), e);
            } finally {
                field.setAccessible(accessible);
            }
        });

        if (hasParent(clazz)) {
            injectInto(clazz.getSuperclass(), object);
        }
    }

    @SuppressWarnings("unchecked")
    private static Iterator<Field> getInjectableFields(Class<?> clazz) {
        return Arrays.asList(clazz.getDeclaredFields()).stream().filter((field) -> field.getAnnotation(Inject.class) != null).iterator();
    }

    private static Class<?> getGenericClazz(Class<?> clazz, Field field) throws ClassNotFoundException {
        String fieldType = field.getType().getTypeName();
        String fullFieldType = field.getGenericType().getTypeName();
        String genericType = fullFieldType.replaceAll(fieldType, "").replaceAll("<", "").replaceAll(">", "");
        return clazz.getClassLoader().loadClass(genericType);
    }

    private static void executePostConstructor(Class<?> clazz, Object object) {
        asList(clazz.getDeclaredMethods())
                .stream()
                .filter((method) -> method.getDeclaredAnnotation(PostConstruct.class) != null)
                .forEach((method) -> {
                    try {
                        method.invoke(object);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                });

        if (hasParent(clazz)) {
            executePostConstructor(clazz.getSuperclass(), object);
        }
    }

    private static boolean hasParent(Class<?> clazz) {
        return clazz.getSuperclass() != null && clazz.getSuperclass() != Object.class;
    }

    static Collection<Object> getObjects() {
        return unmodifiableCollection(singletons);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(Class<T> clazz) {
        List<T> objects = getBeans(clazz);

        if (objects.size() > 1) {
            StringBuilder message = new StringBuilder("Multiple beans " + clazz.getName() + " found : ");
            objects.stream().forEach((object) -> message.append(object.getClass().getName()).append(", "));
            throw new IllegalArgumentException(message.delete(message.length() - 2, message.length()).toString());
        }
        return objects.get(0);
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> getBeans(Class<T> clazz) {
        List<T> objects = singletons.stream()
                                    .parallel()
                                    .filter(clazz::isInstance)
                                    .map((object) -> (T)object)
                                    .collect(Collectors.toList());

        if (objects.size() == 0) {
            throw new IllegalArgumentException("Missing bean for " + clazz.getName());
        }
        return objects;
    }

    public static void register(Object object) {
        if (singletons.contains(object)) {
            throw new IllegalArgumentException("Object already registered");
        }
        singletons.add(object);
    }

    public static void unregister(Object object) {
        if (!singletons.contains(object)) {
            throw new IllegalArgumentException("Object not registered");
        }
        singletons.remove(object);
    }
}