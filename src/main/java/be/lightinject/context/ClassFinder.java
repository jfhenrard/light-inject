package be.lightinject.context;

import java.io.File;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.RecursiveTask;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static java.nio.file.Files.isDirectory;

class ClassFinder extends RecursiveTask<List<Class<?>>> {
    private final String path;
    private final String packageName;
    private final Predicate<Class<?>> classFilter;

    ClassFinder(String path, String packageName, Predicate<Class<?>> classFilter) {
        this.path = path;
        this.packageName = packageName;
        this.classFilter = classFilter;
    }

    @Override
    protected List<Class<?>> compute() {
        if (isJarFile(path)) {
            return parseJar();
        }

        Path currentPath = new File(path).toPath();
        if (!Files.exists(currentPath)) {
            return new ArrayList<>();
        }

        Set<Class<?>> classes = new HashSet<>();
        List<ClassFinder> finders = new ArrayList<>();
        try(DirectoryStream<Path> entries = Files.newDirectoryStream(currentPath)) {
            for(Path entry : entries){
                if(isDirectory(entry.toAbsolutePath())){
                    ClassFinder finder = new ClassFinder(entry.toAbsolutePath().toString(), packageName, classFilter);
                    finders.add(finder);
                    finder.fork();
                } else if (entry.toString().endsWith(".class")) {
                    Class<?> clazz = toClass(toClassName(entry));
                    if (clazz != null && isAcceptable(clazz)) {
                        classes.add(clazz);
                    }
                }
            }
        } catch(Exception e) {
            throw new RuntimeException(e);
        }

        for(ClassFinder finder : finders) {
            classes.addAll(finder.join());
        }
        return new ArrayList<>(classes);
    }

    private boolean isAcceptable(Class<?> clazz) {
        return classFilter.test(clazz);
    }

    private String toClassName(Path entry) {
        String className = entry.toString().replaceAll("[.]class", "").replace('/', '.').replace('\\', '.');
        int startOfClassName = className.lastIndexOf(packageName) + packageName.length();
        className = packageName + className.substring(startOfClassName, className.length());
        return className;
    }

    private List<Class<?>> parseJar() {
        List<Class<?>> classes = new ArrayList<>();

        try {
            URL jar = pathToUrl();
            ZipInputStream zip = new ZipInputStream(jar.openStream());
            ZipEntry entry;
            while ((entry = zip.getNextEntry()) != null) {
                if (entry.getName().endsWith(".class")) {
                    String className = entry.getName().replaceAll("[$].*", "").replaceAll("[.]class", "").replace('/', '.');
                    if (className.startsWith(packageName)) {
                        Class<?> clazz = toClass(className);
                        if (clazz != null && isAcceptable(clazz)) {
                            classes.add(clazz);
                        }
                    }
                }
            }
        } catch(Exception e) {
            throw new RuntimeException(e);
        }

        return classes;
    }

    private URL pathToUrl() {
        try {
            String[] split = path.split("!");
            return new URL(split[0]);
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean isJarFile(String path) {
        return path.startsWith("file:") && path.contains("!");
    }

    private Class<?> toClass(String className) {
        try {
            return Class.forName(className);
        } catch(NoClassDefFoundError e) {
            // Impossible to load class => ignore it
            return null;
        } catch(Throwable e) {
            throw new RuntimeException("Exception in load class " + className, e);
        }
    }
}
