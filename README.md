Light Inject
=============

It's a very light and simple dependency injection framework. Use only Java introspection to discover bean (no dependency needed to use this library).

Currently support only annotations `@Named`, `@Inject` and `@PostConstruct` without arguments. A inject on ancestors list is supported.

Builed for Java 8 (use lambda expression and collection stream).


Installation
-----------

    $ mvn install


Usage
-----

1. Context
	
	Call `Context.packageScan(<packageName>)` to parse beans
	
	Call `Context.getBean(<ClassName>)` to get a bean
	
	Call `Context.getBeans(<AncestorClassName>)` to get a collection of beans
	
	
2. PackageScanner
	
	You can use to PackageScanner to find a list of classes with somes criterias
	
	`PackageScanner.packageScanner().from(<packageName>).with(<predicate>).scan()`
	
	Supported Predicates are :
		- `classAnnotation(<AnnotationClass>)`
		- `descendantOf(<className>)`
		- `notPrivate()`
	
	
See tests to more explanations

Testing
-------

To run the tests:

    $ mvn test


Contributing
------------

1. Fork it.
2. Create a branch (`git checkout -b my_markup`)
3. Commit your changes (`git commit -am "Added Snarkdown"`)
4. Push to the branch (`git push origin my_markup`)
5. Open a [Pull Request]
6. Enjoy a refreshing and wait
